# Data File to Raw Incoming Data

To be used for conversion of *.data files from client to archiver*.in data for server processing. Saves time so we don't have to set up a client machine to send data to the server. (Can also be modified for general renaming).

## Getting started

Not an optimized code because everything is static inside the code and does not accept args. Done in a lazy frenzy.
